import com.fasterxml.jackson.annotation.JsonProperty;

public class Parameter {
    @JsonProperty("id")
    private int id;
    @JsonProperty("type")
    private int type;
    @JsonProperty("label")
    private String label;
    @JsonProperty("description")
    private String description;
    @JsonProperty("k_factor")
    private double k_factor;
    @JsonProperty("b_factor")
    private double b_factor;
    @JsonProperty("units")
    private String units;
    @JsonProperty("smc")
    private String smc;
    @JsonProperty("address")
    private String address;
    @JsonProperty("notification_value")
    private double notification_value;
    @JsonProperty("warning_value")
    private double warning_value;
    @JsonProperty("maximum_value")
    private double maximum_value;
    @JsonProperty("minimum_value")
    private double minimum_value;
    @JsonProperty("default_value")
    private double default_value;
    @JsonProperty("payload_position")
    private int payload_position;
    @JsonProperty("payload_size")
    private int payload_size;
    @JsonProperty("mask")
    private String mask;
    @JsonProperty("shift")
    private int shift;
    @JsonProperty("dac")
    private int dac;


    Parameter() {}

    Parameter(int id, int type, String label, String description, double k_factor, double b_factor, String units,
              String smc, String address, double notification_value, double warning_value, double maximum_value,
              double minimum_value, double default_value, int payload_position, int payload_size, String mask, int shift, int dac) {
        super();
        this.id = id;
        this.type = type;
        this.label = label;
        this.description = description;
        this.k_factor = k_factor;
        this.b_factor = b_factor;
        this.units = units;
        this.smc = smc;
        this.address = address;
        this.notification_value = notification_value;
        this.warning_value = warning_value;
        this.maximum_value = maximum_value;
        this.minimum_value = minimum_value;
        this.default_value = default_value;
        this.payload_position = payload_position;
        this.payload_size = payload_size;
        this.mask = mask;
        this.shift = shift;
        this.dac = dac;
    }

}

