import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.util.ArrayList;
import java.util.List;

public class Parameters {

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Parameter> parameter;

    public Parameters() {}

    public Parameters(List<Parameter> parameters) {
        this.parameter = parameters;
    }

    public List<Parameter> getParameter()
    {
        return parameter;
    }

    public void setParameter(List<Parameter> parameter)
    {
        this.parameter = parameter;
    }
}
