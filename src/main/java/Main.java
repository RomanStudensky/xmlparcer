import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.*;
import java.util.List;

public class Main {

    public static void main(String[] args)
    {
        XmlMapper xmlMapper = null;
        String xml = null;
        try {
            File file = new File("C:\\Users\\Admin\\xmlM\\configParameters.xml");
            xmlMapper = new XmlMapper();
            xml = inputStreamToString(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parameters value = null;
        try {
            value = xmlMapper.readValue(xml, Parameters.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(value.getParameter().get(0).toString());

    }

    public static String inputStreamToString(InputStream is) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}